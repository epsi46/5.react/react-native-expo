// Images Pixabay: https://pixabay.com/fr/users/jmexclusives-10518280/

const PLANTATIONS = [
  {
    id: '1',
    title: 'Plantation de roses à gardienner',
    description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae, consequatur dolor sequi provident possimus sed ad qui ipsum similique odio iste molestias praesentium nemo ut facere, consectetur error in voluptate!',
    image: 'https://cdn.pixabay.com/photo/2014/04/14/20/11/pink-324175_1280.jpg',
    price: 99.99,
    selected: false,
    instructorId: '1'
  }, 
  {
    id: '2',
    title: 'Plantation de marguerites à gardienner',
    description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae, consequatur dolor sequi provident possimus sed ad qui ipsum similique odio iste molestias praesentium nemo ut facere, consectetur error in voluptate!',
    image: 'https://cdn.pixabay.com/photo/2021/05/26/22/33/daisies-6286585_960_720.jpg',
    price: 49.99,
    selected: false,
    instructorId: '1'
  },
  {
    id: '3',
    title: 'Plantation de tullipes à gardienner',
    description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae, consequatur dolor sequi provident possimus sed ad qui ipsum similique odio iste molestias praesentium nemo ut facere, consectetur error in voluptate!',
    image: 'https://cdn.pixabay.com/photo/2017/06/17/23/29/tullips-2414035_1280.jpg',
    price: 29.49,
    selected: false,
    instructorId: '1'
  }, 
  {
    id: '4',
    title: 'Plantation de fushias à gardienner',
    description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae, consequatur dolor sequi provident possimus sed ad qui ipsum similique odio iste molestias praesentium nemo ut facere, consectetur error in voluptate!',
    image: 'https://cdn.pixabay.com/photo/2018/09/15/19/41/fushia-3680172_960_720.jpg',
    price: 49.29,
    selected: false,
    instructorId: '1'
  },
  {
    id: '5',
    title: 'Plantation de framboisier à gardienner',
    description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae, consequatur dolor sequi provident possimus sed ad qui ipsum similique odio iste molestias praesentium nemo ut facere, consectetur error in voluptate!',
    image: 'https://cdn.pixabay.com/photo/2016/09/28/13/24/berries-of-a-raspberry-1700485_960_720.jpg',
    price: 199.99,
    selected: false,
    instructorId: '1'
  },
  {
    id: '6',
    title: 'Plantation d\'anémones à gardienner',
    description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae, consequatur dolor sequi provident possimus sed ad qui ipsum similique odio iste molestias praesentium nemo ut facere, consectetur error in voluptate!',
    image: 'https://cdn.pixabay.com/photo/2022/08/07/11/49/anemone-7370430_960_720.jpg',
    price: 9.99,
    selected: false,
    instructorId: '1'
  },
  {
    id: '7',
    title: 'Plantation d\'eucalyptus à gardienner',
    description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae, consequatur dolor sequi provident possimus sed ad qui ipsum similique odio iste molestias praesentium nemo ut facere, consectetur error in voluptate!',
    image: 'https://cdn.pixabay.com/photo/2014/03/02/05/27/aloe-vera-277948_960_720.jpg',
    price: 29.99,
    selected: false,
    instructorId: '1'
  },
  {
    id: '8',
    title: 'Plantation de jasmin à gardienner',
    description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae, consequatur dolor sequi provident possimus sed ad qui ipsum similique odio iste molestias praesentium nemo ut facere, consectetur error in voluptate!',
    image: 'https://cdn.pixabay.com/photo/2020/06/06/15/48/scent-of-jasmine-5267074_960_720.jpg',
    price: 19.49,
    selected: false,
    instructorId: '1'
  },
  {
    id: '9',
    title: 'Plantation de lilas à gardienner',
    description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae, consequatur dolor sequi provident possimus sed ad qui ipsum similique odio iste molestias praesentium nemo ut facere, consectetur error in voluptate!',
    image: 'https://cdn.pixabay.com/photo/2019/03/03/19/22/lilac-4032657_960_720.jpg',
    price: 29.45,
    selected: false,
    instructorId: '1'
  }
];

export default PLANTATIONS;