import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import globalStyles from '../styles/globalStyles'

const PlantationItem = (props) => {
    return (
        <View style={styles.plantationContainer}>
            <View style={styles.imageContainer}>
                <Image
                    source={{ uri: props.image }}
                    style={styles.image}
                />
            </View>
            <View style={styles.plantationContainerDetails}>
                <Text style={styles.plantationTitle}>{props.title}</Text>
                <Text style={styles.plantationPrice}>{props.price.toFixed(2)}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    plantationContainer: {
        backgroundColor: globalStyles.white,
        borderRadius: 10,
        height: 300,
        margin: 25,
        borderColor: globalStyles.lightGrey,
        borderWidth: 1
    },
    imageContainer: {
        width: '100%',
        height: '60%'
    },
    image: {
        width: '100%',
        height: '100%'
    },
    plantationContainerDetails: {
        alignItems: 'center',
        height: '20%',
        padding: 10
    },
    plantationTitle: {
        fontSize: 18,
        marginVertical: 4,
        color: globalStyles.green,
        fontWeight: 'bold',
        textTransform: 'uppercase'
    },
    plantationPrice: {
        color: globalStyles.darkGrey,
        fontSize: 16
    }
})

export default PlantationItem
