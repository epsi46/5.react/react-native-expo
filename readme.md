# Local dependencies :

npm i @expo/ngrok
npm install redux react-redux


# Docker init :
docker build -t react_native_app .

if no docker compose :
    docker run -it --rm --name react_native_app \
           -p 19006:19006 \
           -v (pwd):/opt/react_native_app/app:delegated \
           -v notused:/opt/react_native_app/app/node_modules \
           react_native_app
else :
docker-compose up -d