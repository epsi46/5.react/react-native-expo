import React from 'react';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import store from './redux/store';
import Landing from './screens/Landing';
import EspacePerso from './screens/Landing';

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Tab.Navigator>
          <Tab.Screen name="Home" component={Landing} />
          <Tab.Screen name="Espace Perso" component={EspacePerso} />
        </Tab.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
