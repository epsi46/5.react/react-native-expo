import { createStore, combineReducers } from "redux";
import reducerPlantations from "./reducers/reducerPlantations";
import reducerEspacePerso from "./reducers/reducerEspacePerso";

const rootReducer = combineReducers({
  plantations: reducerPlantations,
  espacePerso: reducerEspacePerso
});

const store = createStore(rootReducer);

export default store;