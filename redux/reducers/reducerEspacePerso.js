import USER from "../../data/userData";

const initialState = {
    existingUser: USER
}
  
const reducerEspacePerso = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
  
export default reducerEspacePerso;
  