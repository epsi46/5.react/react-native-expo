import React from 'react';
import { Text, View } from 'react-native';
import { useSelector } from 'react-redux';

const EspacePerso = ({ route }) => {
  const espacePersoText = useSelector(state => state.espacePerso.text);

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      {route.name === 'Espace Perso' && (
        <Text>{espacePersoText}</Text>
      )}
    </View>
  );
};

export default EspacePerso;
