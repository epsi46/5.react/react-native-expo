import { FlatList, StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { useSelector } from 'react-redux';
import PlantationItem from '../components/PlantationItem';

const Landing = ({ route }) => {
  const existingPlantations = useSelector(state => state.plantations.existingPlantations);

  return (
    <View style={styles.container}>
      {route.name === 'Home' && (
        <FlatList
          data={existingPlantations}
          renderItem={({ item }) => (
            <PlantationItem
              image={item.image}
              title={item.title}
              price={item.price}
            />
          )}
          keyExtractor={(item) => item.id.toString()}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Landing;
